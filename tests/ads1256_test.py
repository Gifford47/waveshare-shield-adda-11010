# tests/ads1256_test.py
#
# Run tests with 'python -m pytest' in main directory
#
# Copyright (c) 2022 Spring City Solutions LLC and other contributors
# Licensed under the Apache License, Version 2.0

import pytest

import ads1256

def test_twos_complement_to_integer_24bit():
    assert ads1256.twos_complement_to_integer_24bit(0x7FFFFF) == 8388607
    assert ads1256.twos_complement_to_integer_24bit(0x000010) == 16
    assert ads1256.twos_complement_to_integer_24bit(0x000001) == 1
    assert ads1256.twos_complement_to_integer_24bit(0x000000) == 0
    assert ads1256.twos_complement_to_integer_24bit(0xFFFFFF) == -1
    assert ads1256.twos_complement_to_integer_24bit(0xFFFFF0) == -16
    assert ads1256.twos_complement_to_integer_24bit(0x800001) == -8388607
    assert ads1256.twos_complement_to_integer_24bit(0x800000) == -8388608

def test_parse_args_default():
    parser = ads1256.parse_args([])
    assert ads1256.register_data['mux'] == 0x08
    assert ads1256.register_data['adcon'] == 0x20
    assert ads1256.register_data['drate'] == 0x03
    assert ads1256.register_data['status'] == 0x00

def test_parse_args_port():
    parser = ads1256.parse_args(['--port', '0'])
    assert ads1256.register_data['mux'] == 0x08
    parser = ads1256.parse_args(['--port', '1'])
    assert ads1256.register_data['mux'] == 0x18
    parser = ads1256.parse_args(['--port', '2'])
    assert ads1256.register_data['mux'] == 0x28
    parser = ads1256.parse_args(['--port', '3'])
    assert ads1256.register_data['mux'] == 0x38
    parser = ads1256.parse_args(['--port', '4'])
    assert ads1256.register_data['mux'] == 0x48
    parser = ads1256.parse_args(['--port', '5'])
    assert ads1256.register_data['mux'] == 0x58
    parser = ads1256.parse_args(['--port', '6'])
    assert ads1256.register_data['mux'] == 0x68
    parser = ads1256.parse_args(['--port', '7'])
    assert ads1256.register_data['mux'] == 0x78

def test_parse_args_gain():
    parser = ads1256.parse_args(['--gain', '1'])
    assert ads1256.register_data['adcon'] == 0x20
    parser = ads1256.parse_args(['--gain', '2'])
    assert ads1256.register_data['adcon'] == 0x21
    parser = ads1256.parse_args(['--gain', '4'])
    assert ads1256.register_data['adcon'] == 0x22
    parser = ads1256.parse_args(['--gain', '8'])
    assert ads1256.register_data['adcon'] == 0x23
    parser = ads1256.parse_args(['--gain', '16'])
    assert ads1256.register_data['adcon'] == 0x24
    parser = ads1256.parse_args(['--gain', '32'])
    assert ads1256.register_data['adcon'] == 0x25
    parser = ads1256.parse_args(['--gain', '64'])
    assert ads1256.register_data['adcon'] == 0x26

def test_parse_args_rate():
    parser = ads1256.parse_args(['--rate', '2.5'])
    assert ads1256.register_data['drate'] == 0x03
    parser = ads1256.parse_args(['--rate', '5'])
    assert ads1256.register_data['drate'] == 0x13
    parser = ads1256.parse_args(['--rate', '10'])
    assert ads1256.register_data['drate'] == 0x23
    parser = ads1256.parse_args(['--rate', '15'])
    assert ads1256.register_data['drate'] == 0x33
    parser = ads1256.parse_args(['--rate', '25'])
    assert ads1256.register_data['drate'] == 0x43
    parser = ads1256.parse_args(['--rate', '30'])
    assert ads1256.register_data['drate'] == 0x53
    parser = ads1256.parse_args(['--rate', '50'])
    assert ads1256.register_data['drate'] == 0x63
    parser = ads1256.parse_args(['--rate', '60'])
    assert ads1256.register_data['drate'] == 0x72
    parser = ads1256.parse_args(['--rate', '100'])
    assert ads1256.register_data['drate'] == 0x82
    parser = ads1256.parse_args(['--rate', '500'])
    assert ads1256.register_data['drate'] == 0x92
    parser = ads1256.parse_args(['--rate', '1000'])
    assert ads1256.register_data['drate'] == 0xA1
    parser = ads1256.parse_args(['--rate', '2000'])
    assert ads1256.register_data['drate'] == 0xB0
    parser = ads1256.parse_args(['--rate', '3750'])
    assert ads1256.register_data['drate'] == 0xC0
    parser = ads1256.parse_args(['--rate', '7500'])
    assert ads1256.register_data['drate'] == 0xD0
    parser = ads1256.parse_args(['--rate', '15000'])
    assert ads1256.register_data['drate'] == 0xE0
    parser = ads1256.parse_args(['--rate', '30000'])
    assert ads1256.register_data['drate'] == 0xF0

def test_parse_args_buffer():
    parser = ads1256.parse_args(['--buffer', 'off'])
    assert ads1256.register_data['status'] == 0x00
    parser = ads1256.parse_args(['--buffer', 'on'])
    assert ads1256.register_data['status'] == 0x02 

def test_parse_args_sdcs():
    parser = ads1256.parse_args(['--sdcs', 'off'])
    assert ads1256.register_data['adcon'] == 0x20
    parser = ads1256.parse_args(['--sdcs', '0.5'])
    assert ads1256.register_data['adcon'] == 0x28
    parser = ads1256.parse_args(['--sdcs', '2'])
    assert ads1256.register_data['adcon'] == 0x30
    parser = ads1256.parse_args(['--sdcs', '10'])
    assert ads1256.register_data['adcon'] == 0x38
