node-red-contrib-waveshare-shield-adda-11010
=====================

A set of <a href="http://nodered.org">Node-RED</a> nodes to control the AD and DA converters
on a
<a href="https://www.waveshare.com/wiki/High-Precision_AD/DA_Board">
Waveshare AD/DA raspberry pi shield</a>

This node provides plug and play use of the hardware on the shield.

Simply set the jumpers on the shield, plug in the shield,
enable SPI port on the Raspberry Pi, install some python libraries,
install this Node-RED node, then every pin on the board just works
with no further configuration required.

See the <a href="https://gitlab.com/SpringCitySolutionsLLC/waveshare-shield-adda-11010/-/wikis/home">wiki</a> for more details.

## Install

Either use the Node-RED Menu - Manage Palette option to install, or run the following
command in your Node-RED user directory - typically `~/.node-red`

        npm i node-red-contrib-waveshare-shield-adda-11010

Enable the SPI port.  On a Raspberry Pi it would look like this:

        sudo raspi-config

Enable the SPI interface.  Option "3 Interfacing Options".  Option "P4 SPI".
Enable "Yes".  Probably have to reboot.

Then install python and pip:

        sudo apt-get install python3-pip
        pip3 install --upgrade pip --user

You will probably have to reboot at this time for ~/.local/bin to be 
automatically added to your path.

Now install, or upgrade the python rpi.gpio and spidev libraries using the script at:

        ~/.node-red/node_modules/node-red-contrib-waveshare-shield-adda-11010/upgrade_pip.sh

or manually:

        pip install --upgrade rpi.gpio --user
        pip install --upgrade spidev --user

Finally after the node is installed, SPI is enabled, python and pip are 
installed, and rpi.gpio and spidev are installed, you'll probably have to 
do one final reboot before the node works.

On the hardware I would suggest installing the shorting horizontal jumper
between AINCOM and AGND unless your application has an unusual need for a
floating ground input.

I would suggest on a Raspberry Pi installing the vertical right side
jumper between Vcc and 3V3 and the adjacent vertical right side jumper
between Vref and 3V3.  The Vref on that jumper refers to the DAC converter
the ADC converter has a hard wired LM285 voltage reference.

For initial testing and experimentation purposes I would suggest installing
the vertical jumpers on the right side connecting LEDB to DAC1, LEDA to DAC0,
AD1 to LDR (light sensor), and AD0 to ADJ (adjustable on board potentiometer).

There are working example flows for all ports.  In Node-RED, click the right
side hamburger menu, "import", "Examples", "node-red-contrib-waveshare-shield-adda-11010"

Some demo flows may require jumper wires to be installed, for example, from DAC1 to LEDB.

## Usage

#### DAC Input

 - `msg.payload` - *number | string*

`msg.payload` out of range high or low will be rounded.

`msg.payload` scale for maximum can be set to 3.3, or 5.0 to match Vref hardware jumper.

See help file for more detail.

#### ADC Output

 - `msg.payload` - *any*

`msg.payload` any input triggers the start of an analog to digital conversion.

About two seconds after the conversion is triggered, a numeric voltage will be output.

See help file for more detail.

#### Digital Inputs

 - `msg.payload` - *number*

`msg.payload` any input triggers the start of an digital read.

A numeric binary 1 or 0 will be output based on the pin voltage.

The pin will be set to input mode.

See help file for more detail.

#### Digital Outputs

 - `msg.payload` - *number*

`msg.payload` a binary 1 or 0 sets the pin voltage Vcc or zero.

The pin will be set to output mode.

See help file for more detail.

## Links

Hardware Links:

Board Manufacturer Store:
https://www.waveshare.com/High-Precision-AD-DA-Board.htm

Board Manufacturer Wiki:
https://www.waveshare.com/wiki/High-Precision_AD/DA_Board

Links for ADS1256 chip:
https://www.ti.com/product/ADS1256
https://www.ti.com/lit/gpn/ads1256

Book "Fundamentals of Precision ADC Noise Analysis" https://www.ti.com/lit/pdf/slyy192

Journal Series "How delta-sigma ADCs work" https://www.ti.com/lit/pdf/slyt423 https://www.ti.com/lit/pdf/slyt438

App Note "Digital Filter Types in Delta-Sigma ADCs" https://www.ti.com/lit/pdf/sbaa230

Link for LM285 chip, connected to the ADS1256:
https://www.ti.com/product/LM285-2.5-N
https://www.ti.com/lit/gpn/lm285-2.5-n

Link for DAC8532 chip:
https://www.ti.com/product/DAC8532
https://www.ti.com/lit/gpn/dac8532

Spring City Solutions Links:

Spring City Solutions Node-RED project page:
https://www.springcitysolutions.com/nodered

Spring City Solutions page for this node:
https://www.springcitysolutions.com/nodered-waveshare-adda-shield

Spring City Solutions Node-RED project email:
nodered@springcitysolutions.com

Spring City Solutions Gitlab for this node:
https://gitlab.com/SpringCitySolutionsLLC/waveshare-shield-adda-11010

Patreon Page:
https://www.patreon.com/springcitysolutions_nodered

Software Links:

Spring City Solutions Gitlab for this node:
https://gitlab.com/SpringCitySolutionsLLC/waveshare-shield-adda-11010

Doxygen docs for this node autogenerated by Gitlab CI/CD:
https://springcitysolutionsllc.gitlab.io/waveshare-shield-adda-11010/index.html

npmjs for this node:
https://npmjs.org/package/node-red-contrib-waveshare-shield-adda-11010

Node-RED flows for this node:
https://flows.nodered.org/node/node-red-contrib-waveshare-shield-adda-11010

Documentation Links:

Gitlab wiki for this node (the master copy of list of links is here):
https://gitlab.com/SpringCitySolutionsLLC/waveshare-shield-adda-11010/-/wikis/home

Waveshare's libraries for this hardware:
https://github.com/waveshare/High-Precision-AD-DA-Board

Youtube video "How to set up":
?

Youtube video "How to use":
?

Youtube video "Testing Results":
?

## Trademarks

WAVESHARE (tm) is a trademark of Shenzhen Weixue Electronic Co., Ltd.

Raspberry Pi is a trademark of Raspberry Pi Trading

Node-RED and node.js are a trademark of the OpenJS Foundation in the United States

Python is a registered trademark of the Python Software Foundation.

"All trademarks are property of their respective owners"

## Copyrights and Licenses

Majority of code Copyright (c) 2020-2022 Spring City Solutions LLC and other
contributors and licensed under the Apache License, Version 2.0

Icon made from http://www.onlinewebfonts.com/icon aka Icon Fonts is licensed by CC BY 3.0
